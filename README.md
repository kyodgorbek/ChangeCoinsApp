# ChangeCoinsApp



import javax.swing.JOptionPane;

public class ChangeCoinsApp {
 
  public static void main(String[] args){
 
 // create a CoinChanger object
 CoinChanger cC = new CoinChanger();
 
 // Store the number of each kind of coin  in the  Coin Changer object
 String numStr = JOptionPane.showInputDialog("Number of pennies");
 int num = Integer.parseInt(numStr);
 cC.setPennies(num);
 
  numStr = JOptionPane.showInputDialog("Number of nickels");
  num = Integer.parseInt(numStr);
  cC.setNickels(num);
  
  numStr = JOptionPane.showInputDialog("Number of dimes");
  num = Integer.parseInt(numStr);
  cC.setDimes(num);
  
  // Display the value of the coins in dollars and change.
  String message = cC.toString() + "\nCoin collection value is $" + cC.findDollars() + "." + cC.findChange();
 }
}  
